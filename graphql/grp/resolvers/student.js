const Student = require("../../models/Student");
const checkAuth = require("../../util/checkauth");
const User = require("../../models/User");
const {
  ApolloServerPluginLandingPageProductionDefault,
} = require("apollo-server-core");

module.exports = {
  Query: {
    async getStudents() {
      try {
        const student = await Student.find().populate("user");
        return student;
      } catch (err) {
        throw new Error(err);
      }
    },
    async getStudent(_, { stuId }) {
      try {
        const student = await Student.findById(stuId).populate("user");
        if (!student) {
          throw new Error("Student not found");
        }
        return student;
      } catch (err) {
        throw new Error(err);
      }
    },
  },
  Mutation: {
    async createStudent(_, { cordinators, student }, context) {
      const user = checkAuth(context);

      const user1 = await User.findById(user.id);

      if (user1.roles !== "admin") {
        throw new Error("can be created only by admin");
      } else {
        if (cordinators.trim() === "") {
          throw new Error("Cordinators should not be empty");
        }
        if (student.trim() === "") {
          throw new Error("Cordinators should not be empty");
        }
        const newStudent = await Student.create({
          cordinators,
          student,
          user: user.id,
        });
        return newStudent.populate("user");
      }
    },
    async updateStudent(_, { stuId, student, cordinators }, context) {
      let user = checkAuth(context);
      user = await User.findById(user.id);

      if (user.roles !== "admin") {
        throw new Error("can be updated only by admin");
      } else {
        if (student.trim() === "") {
          throw new Error("student should not be empty");
        }
        const student1 = await Student.findById(stuId);
        if (!student1) {
          throw new Error("Student does not exists");
        }
        const newstudent = await Student.findByIdAndUpdate(
          stuId,
          { $set: { student, cordinators } },
          {
            new: true,
            runValidators: true,
          }
        );

        return newstudent.populate("user");
      }
    },
    async deleteStudent(_, { stuId }, context) {
      let user = checkAuth(context);
      user = await User.findById(user.id);

      if (user.roles !== "admin") {
        throw new Error("can be updated only by admin");
      } else {
        const student = await Student.findById(stuId);

        if (!student) {
          throw new Error("Student does not exists");
        }
        await Student.deleteOne({ stuId });

        return "deleted successfuly";
      }
    },
    async createSubmission(_, { stuId }, context) {
      const user = checkAuth(context);
      const user1 = await User.findById(user.id);

      if (user1.roles !== "admin") {
        throw new Error("can be created only by admin");
      } else {
        const student = await Student.findById(stuId);
        if (!student) {
          throw new Error("Student not found");
        }
        student.submissions.unshift({});

        await student.save();
        return student.populate("user");
      }
    },
    async updateSubmission(_, { stuId, subId, submission }, context) {
      const user = checkAuth(context);
      const user1 = await User.findById(user.id);

      if (user1.roles !== "admin") {
        throw new Error("can be created only by admin");
      } else {
        const student = await Student.findById(stuId);
        if (!student) {
          throw new Error("Student not found");
        }
        const subIndex = student.submissions.findIndex((c) => c.id === subId);
        student.submissions[subIndex] = { submission };
        await student.save();
        return student.populate("user");
      }
    },
    async deleteSubmission(_, { stuId, subId }, context) {
      const user = checkAuth(context);
      const user1 = await User.findById(user.id);

      if (user1.roles !== "admin") {
        throw new Error("can be created only by admin");
      } else {
        const student = await Student.findById(stuId);
        if (!student) {
          throw new Error("Student not found");
        }
        const subIndex = student.submissions.findIndex((c) => c.id === subId);
        student.submissions.splice(subIndex, 1);
        await student.save();
        return "deleted successfuly";
      }
    },
  },
};
