const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const mongoose = require("mongoose");
require("dotenv").config();

const typeDefs = require("./grp/typeDefs");
const resolvers = require("./grp/resolvers");

app = express();
path = "/graphql";
const port = process.env.PORT;

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({ req }),
});
// async function apolloserver() {
//   try {
//     await server.start();
//     server.applyMiddleware({ app, path });
//   } catch (err) {
//     console.log(err);
//   }
// }
// apolloserver();
// mongoosedB();
server
  .start()
  .then(() => {
    server.applyMiddleware({ app, path });
  })
  .catch((err) => {
    console.log(err);
  });

// async function mongoosedB() {
//   try {
//     await mongoose.connect(process.env.MONGO_URI);
//     console.log("Db running");
//     return app.listen(port);
//   } catch (err) {
//     console.log(err);
//   }
// }

mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    console.log("Db running");
    return app.listen(port);
  })
  .catch((err) => {
    console.log(err);
  });
