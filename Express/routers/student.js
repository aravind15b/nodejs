const express = require("express");
const {
  getStudents,
  createStudent,
  getStudent,
  updateStudent,
  deleteStudent,
} = require("../controllers/student");
const submission = require("./submission");

// const Student = require("../models/Student");

const router = express.Router();

router.use("/:studentId/submissions", submission);

router.route("/").get(getStudents).post(createStudent);
router.route("/:id").get(getStudent).put(updateStudent).delete(deleteStudent);
module.exports = router;
