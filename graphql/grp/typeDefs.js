const { gql } = require("apollo-server-express");

module.exports = gql`
  type User {
    id: ID!
    email: String!
    password: String!
    username: String!
    token: String!
    createdAt: String
    updatedAt: String
    roles: String!
  }
  type Student {
    id: ID!
    cordinators: String!
    student: String!
    user: User!
    submissions: [Submission]
  }
  type Submission {
    id: ID!
    submissionDate: String
    submission: String
  }
  input RegisterInput {
    email: String!
    password: String!
    username: String!
    roles: String
  }
  enum Roles {
    admin
    user
  }
  type Query {
    getStudents: [Student]
    getStudent(stuId: String!): Student
  }
  type Mutation {
    register(data: RegisterInput!): User!
    login(username: String!, password: String!): User!
    createStudent(cordinators: String!, student: String!): Student!
    updateStudent(stuId: ID!, cordinators: String!, student: String!): Student!
    deleteStudent(stuId: ID!): String
    createSubmission(stuId: ID!): Student!
    updateSubmission(stuId: ID!, subId: ID!, submission: String): Student!
    deleteSubmission(stuId: ID!, subId: ID!): String!
  }
`;
