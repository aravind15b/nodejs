const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Submission = require("../models/Submission");
const Student = require("../models/Student");

exports.getSubmissions = asyncHandler(async (req, res, next) => {
  if (req.params.studentId) {
    const submission = await Submission.find({ student: req.params.studentId });
    return res.status(200).json({ success: true, data: submission });
  } else {
    const submission2 = await Submission.find().populate("student");
    return res.status(200).json({ success: true, data: submission2 });
  }
});

exports.createSubmission = asyncHandler(async (req, res, next) => {
  req.body.student = req.params.studentId;
  const student = await Student.findById(req.params.studentId);
  console.log(student);
  if (!student) {
    new ErrorResponse(
      `no id found for that student ${req.body.studentId}`,
      404
    );
  }
  const submission = await Submission.create(req.body);
  res.status(201).json({ success: true, data: submission });
});

exports.getSubmission = asyncHandler(async (req, res, next) => {
  const submission = await Submission.findById(req.params.id);
  if (!submission) {
    return next(new ErrorResponse(`no id is found ${req.params.id}`, 404));
  }
  res.status(201).json({ success: true, data: submission });
});

exports.updateSubmission = asyncHandler(async (req, res, next) => {
  let submission = await Submission.findById(req.params.id);
  if (!submission) {
    return next(new ErrorResponse(`no id is found ${req.params.id}`, 404));
  }
  submission = await Submission.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  res.status(201).json({ success: true, data: submission });
});

exports.deleteSubmission = asyncHandler(async (req, res, next) => {
  let submission = await Submission.findById(req.params.id);
  if (!submission) {
    return next(new ErrorResponse(`no id is found ${req.params.id}`, 404));
  }
  submission.remove();
  res.status(201).json({ success: true, data: [] });
});
