const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Student = require("../models/Student");

exports.getStudents = asyncHandler(async (req, res, next) => {
  const students = await Student.find().populate("submissions");
  res.status(200).json({ success: true, data: students });
});

exports.createStudent = asyncHandler(async (req, res, next) => {
  const student = await Student.create(req.body);
  res.status(201).json({ success: true, data: student });
});

exports.getStudent = asyncHandler(async (req, res, next) => {
  const student = await Student.findById(req.params.id);
  console.log(student);
  console.log(typeof req.params.id);
  if (!student) {
    return next(new ErrorResponse(`no id is found ${req.params.id}`, 404));
  }
  res.status(201).json({ success: true, data: student });
});

exports.updateStudent = asyncHandler(async (req, res, next) => {
  let student = await Student.findById(req.params.id);
  if (!student) {
    return next(new ErrorResponse(`no id is found ${req.params.id}`, 404));
  }
  student = await Student.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  res.status(201).json({ success: true, data: student });
});

exports.deleteStudent = asyncHandler(async (req, res, next) => {
  let student = await Student.findById(req.params.id);
  if (!student) {
    return next(new ErrorResponse(`no id is found ${req.params.id}`, 404));
  }
  student.remove();
  res.status(201).json({ success: true, data: [] });
});
