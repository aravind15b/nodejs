const express = require("express");
const {
  getSubmissions,
  createSubmission,
  getSubmission,
  updateSubmission,
  deleteSubmission,
} = require("../controllers/submission");

const router = express.Router({ mergeParams: true });

router.route("/").get(getSubmissions).post(createSubmission);
router
  .route("/:id")
  .get(getSubmission)
  .put(updateSubmission)
  .delete(deleteSubmission);

module.exports = router;
