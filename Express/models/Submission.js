const mongoose = require("mongoose");

const SubmissionSchema = new mongoose.Schema(
  {
    SubmittedDate: {
      type: Date,
      default: Date.now,
    },
    IsSubmitted: {
      type: Boolean,
      default: false,
    },
    student: {
      type: mongoose.Schema.ObjectId,
      ref: "Student",
      required: true,
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Submission", SubmissionSchema);
