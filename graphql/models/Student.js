const { model, Schema } = require("mongoose");

const studentSchema = new Schema(
  {
    cordinators: {
      type: String,
      require: [true, "enter a cordinator"],
    },
    student: {
      type: String,
      require: [true, "enter a student"],
    },
    submissions: [
      {
        submissionDate: {
          type: Date,
          default: Date.now,
        },
        submission: {
          type: Boolean,
          default: false,
        },
      },
    ],
    user: {
      type: Schema.ObjectId,
      ref: "User",
      required: true,
    },
  },
  { timestamps: true }
);
module.exports = model("Student", studentSchema);
