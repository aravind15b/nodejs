const path = require("path");
const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");

const errorHandler = require("./middleware/error");
const connectDB = require("./config/db");

dotenv.config({ path: "./config/config.env" });

connectDB();

const student = require("./routers/student");
const submission = require("./routers/submission");

const app = express();

app.use(express.json());
app.use(morgan("dev"));

app.use("/api/v1/students", student);
app.use("/api/v1/submissions", submission);
app.use(errorHandler);

const PORT = process.env.PORT;

app.listen(PORT);
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`);
});
