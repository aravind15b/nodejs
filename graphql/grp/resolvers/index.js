const usersResolver = require("./user");
const studentsResolver = require("./student");
module.exports = {
  Query: {
    ...studentsResolver.Query,
  },
  Mutation: {
    ...usersResolver.Mutation,
    ...studentsResolver.Mutation,
  },
};
