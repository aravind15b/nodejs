const mongoose = require("mongoose");

const StudentSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "add name"],
      unique: true,
    },
    coordinates: {
      type: String,
      required: [true, "add the coordinates"],
      unique: true,
    },
  },
  { timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } }
);
StudentSchema.pre("remove", async function (next) {
  console.log(`Deleting submission accoring to ${this._id}`);
  await this.model("Submission").deleteMany({ student: this._id });
});

StudentSchema.virtual("submissions", {
  ref: "Submission",
  localField: "_id",
  foreignField: "student",
  justOne: false,
});

module.exports = mongoose.model("Student", StudentSchema);
